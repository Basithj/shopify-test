"use strict";

const customMegaMenu = {
    hamburger: document.querySelector(".hamburger"),
    navMenu: document.querySelector(".nav-menu"),
    navLink: document.querySelectorAll(".nav-link"),
    topLink: document.querySelectorAll(".top-link--menu"),
    init: function(){
        /*this.navLink.forEach(n => n.addEventListener("click", this.closeMenu));*/
        this.hamburger.addEventListener("click", this.mobileMenu);
        this.topLink.forEach(n => n.addEventListener("click", this.navTopLink));
    },
    navTopLink: function() {
        document.querySelectorAll(".mega-menu-wrapper").forEach(n => {if(n != this.nextElementSibling){n.classList.remove("active")}});
        this.nextElementSibling.classList.toggle("active");
    },
    mobileMenu: function() {
        document.body.classList.toggle('overflow-hidden');
        customMegaMenu.hamburger.classList.toggle("active");
        customMegaMenu.navMenu.classList.toggle("active");
    },
    closeMenu: function() {
        customMegaMenu.hamburger.classList.remove("active");
        customMegaMenu.navMenu.classList.remove("active");
    }
}

customMegaMenu.init();